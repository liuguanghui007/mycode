public class MyThread3 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 50000; i++) {
            System.out.println("i ="+ (i+1));
        }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread3 myThread3 = new MyThread3();
        myThread3.start();

        myThread3.interrupt();
        Thread.sleep(1000);
        //此处的myThread3.interrupted()在源码中调用的是currentThread().isInterrupted(true)，这是Thread的静态方法;
        //其实判断的是当前线程是否被打断，而当前线程指的是main线程，因此运行的结果是false
        System.out.println("是否停止1？"+myThread3.interrupted());
        System.out.println("是否停止2？"+myThread3.interrupted());
        //如果想打断main线程可利用下面的方法,运行后发现第一个为true，第二个为false
        //是因为currentThread().isInterrupted(true)中的true，会清除线程打断的状态
        //当第一次显示true，意味着main被打断，然后清楚main线程的状态，即第二次会变为false
        /*Thread.currentThread().interrupt();
        System.out.println("是否停止1？"+myThread3.interrupted());
        System.out.println("是否停止2？"+myThread3.interrupted());*/
        //也可以改造MyThread4中的run方法来判断myThread3是否被打断，请参阅MyThread4

    }
}
