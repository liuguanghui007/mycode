public class MyThread12 extends Thread{
    private int i=0;
    @Override
    public void run() {
        long begin = System.currentTimeMillis();
        while (i<500000){
            Thread.yield();
            i++;
        }
        long end = System.currentTimeMillis();
        System.out.println("方法花费了："+(end-begin)+"ms");
    }

    public static void main(String[] args) {
        MyThread12 myThread12 = new MyThread12();
        myThread12.start();
    }
}
