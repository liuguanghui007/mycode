public class CountOperate2 extends Thread{
    public CountOperate2() {
        System.out.println("CountOperate----begin");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("Thread.currentThread().isAlive()="+Thread.currentThread().isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("this.getName()="+this.getName()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("this.isAlive()="+this.isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("CountOperate----end");
    }

    @Override
    public void run() {
        System.out.println("CountOperate----begin");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("Thread.currentThread().isAlive()="+Thread.currentThread().isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("this.getName()="+this.getName()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("this.isAlive()="+this.isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        System.out.println("CountOperate----end");
    }

    public static void main(String[] args) {
        CountOperate2 countOperate2 = new CountOperate2();
        Thread t1 = new Thread(countOperate2);
        System.out.println("main begin t1 isAlive="+t1.isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
        t1.setName("A");
        t1.start();
        System.out.println("main end t1 isAlive="+t1.isAlive()+"---Thread.currentThread().getId()="+Thread.currentThread().getId());
    }
}
