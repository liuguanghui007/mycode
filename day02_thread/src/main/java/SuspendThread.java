public class SuspendThread extends Thread{
    private long i=0;

    public long getI() {
        return i;
    }

    public void setI(long i) {
        this.i = i;
    }

    @Override
    public void run() {
        super.run();
        while (true){
            i++;
        }
    }

    public static void main(String[] args) {
        try {
            SuspendThread suspendThread = new SuspendThread();
            suspendThread.start();
            Thread.sleep(5000);
            //A段
            suspendThread.suspend();
            System.out.println("A="+System.currentTimeMillis()+"i="+suspendThread.getI());
            suspendThread.resume();
            Thread.sleep(5000);
            suspendThread.suspend();
            System.out.println("B="+System.currentTimeMillis()+"i="+suspendThread.getI());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
