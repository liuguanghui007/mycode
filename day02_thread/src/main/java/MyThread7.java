public class MyThread7 extends Thread{
    @Override
    public void run() {
        //线程休眠中被打断，会立刻停止线程，并将打断状态清除
        try {
            System.out.println("run begin");
            Thread.sleep(100000);
            System.out.println("run end");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("线程休眠中被打断了");
        }

    }

    public static void main(String[] args) throws InterruptedException {
        MyThread7 myThread7 = new MyThread7();
        myThread7.start();
        //Thread.sleep(1000);
        myThread7.interrupt();
        System.out.println("end");
    }
}
