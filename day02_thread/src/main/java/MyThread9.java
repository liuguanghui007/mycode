public class MyThread9 extends Thread{
    @Override
    public void run() {
        int i=0;
        try {
            while (true){
                System.out.println(i++);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("进入catch块中");
        }

    }

    public static void main(String[] args) {
        MyThread9 myThread9 = new MyThread9();
        try {
            myThread9.start();
            Thread.sleep(8000);
            myThread9.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
