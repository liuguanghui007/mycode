public class MyThread6 extends Thread{
    @Override
    public void run() {
        try {
            for (int i = 0; i < 500000; i++) {
                if(this.interrupted()){
                    System.out.println("线程已经被打断，我要退出了。。。");
                    //break;   break方法不会打断线程，可以通过抛异常来立刻打断线程执行
                    throw new Exception();
                }
                System.out.println(i+1);
            }
            System.out.println("我在for下面");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws InterruptedException {
        MyThread6 myThread6 = new MyThread6();
        myThread6.start();
        Thread.sleep(1000);
        myThread6.interrupt();
        System.out.println("end");
    }
}
