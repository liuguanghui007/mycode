public class Thread11 extends Thread{
    private int i=0;
    @Override
    public void run() {
        while (true){
            i++;
            System.out.println(i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread11 thread11 = new Thread11();
        thread11.start();//线程开始执行，运行run方法中的System.out.println(i);方法时，由于这个方法是加了锁的，导致
        thread11.suspend();
        Thread.sleep(10000);
        System.out.println("main end");
    }
}
