import vo.SynchronizedObject;

public class MyThread10 extends Thread{
    private SynchronizedObject object;

    public MyThread10(SynchronizedObject object) {
        this.object = object;
    }

    @Override
    public void run() {
        super.run();
        object.printString("b","bb");
    }

    public static void main(String[] args) {
        try {
            SynchronizedObject ob = new SynchronizedObject();
            MyThread10 myThread10 = new MyThread10(ob);
            myThread10.start();//赋值username=b之后，线程休眠10s，password还没赋值bb，此时的password仍为aa
            Thread.sleep(500);
            myThread10.stop();//强行停止线程
            System.out.println("Object name="+ob.getUsername()+"-----Object password="+ob.getPassword());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
