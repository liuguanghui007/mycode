public class MyThread4 extends Thread{
    @Override
    public void run() {
        Thread.currentThread().interrupt();
       if(!Thread.currentThread().isInterrupted()){
           System.out.println("当前线程没有被打断。当前线程为："+Thread.currentThread().getName());
       }else{
           System.out.println("当前线程被打断了。当前线程为："+Thread.currentThread().getName());
       }
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread4 myThread4 = new MyThread4();
        Thread.sleep(1000);
        myThread4.start();
    }
}
