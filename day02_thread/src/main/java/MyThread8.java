public class MyThread8 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 10000000; i++) {
            System.out.println(i+1);
        }
        try {
            System.out.println("run begin");
            Thread.sleep(2000);
            System.out.println("run end");
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("进入catch中"+this.isInterrupted());
        }
    }

    public static void main(String[] args) {
        MyThread8 myThread8 = new MyThread8();
        myThread8.start();
        myThread8.interrupt();//此处模拟线程先被打断，然后再休眠
        System.out.println("end");
    }
}
