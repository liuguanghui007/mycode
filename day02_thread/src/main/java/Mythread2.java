public class Mythread2 extends Thread{
    @Override
    public void run() {

        try {
            System.out.println("run  threadName="+this.currentThread().getName()+"begin =" +System.currentTimeMillis() );
            Thread.sleep(2000);
            System.out.println("run  threadName="+this.currentThread().getName()+"end ="+System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Mythread2 mythread2 = new Mythread2();
        System.out.println("begin="+System.currentTimeMillis());
        mythread2.start();
        System.out.println("end="+System.currentTimeMillis());
    }
}
