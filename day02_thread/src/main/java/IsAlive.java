public class IsAlive extends Thread{
    @Override
    public void run() {
        System.out.println("run="+this.isAlive());
    }

    public static void main(String[] args) throws InterruptedException {
        IsAlive isAlive = new IsAlive();
        System.out.println("begin=="+isAlive.isAlive());
        isAlive.start();
        Thread.sleep(2000);
        System.out.println("end=="+isAlive.isAlive());
    }
}
