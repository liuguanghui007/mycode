package com.spring;

import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class SpringTest {
    @Test
    public  void test() {
        ClassPathResource classPathResource = new ClassPathResource("applicationContext.xml");
        //BeanFactory beanFactory=new XmlBeanFactory(classPathResource);
        ClassPathXmlApplicationContext beanFactory=new ClassPathXmlApplicationContext("applicationContext.xml");
        Object user1 = beanFactory.getBean("user");
        Object user2 = beanFactory.getBean("user2");
        System.out.println(user1);
        System.out.println(user2);
        //System.out.println(user1);
    }
    @Test
    public void futureTest() throws Exception {

        System.out.println("main函数开始执行");

        ExecutorService executor = Executors.newFixedThreadPool(1);
        Future<Integer> future = executor.submit(new Callable<Integer>() {
            public Integer call() throws Exception {

                System.out.println("===task start===");
                Thread.sleep(5000);
                System.out.println("===task finish===");
                return 3;
            }
        });
        //这里需要返回值时会阻塞主线程，如果不需要返回值使用是OK的。倒也还能接收
        Integer result=future.get();
        System.out.println(result);
        System.out.println("main函数执行结束");



    }
}
