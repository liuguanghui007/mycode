package com.spring.vo;

public class User {


    public User() {
        System.out.println("构造方法执行了");
    }

    public void init(){
        System.out.println("初始化方法执行了");
    }

    public void destroy(){
        System.out.println("销毁方法执行了");
    }

}
