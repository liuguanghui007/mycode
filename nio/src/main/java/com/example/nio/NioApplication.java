package com.example.nio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class NioApplication {

    public static void main(String[] args) {
        SpringApplication.run(NioApplication.class, args);
    }

}
