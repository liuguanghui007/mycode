package com.example.nio.com.lgh.controller;

import com.example.nio.com.lgh.service.AsyncServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/async")
@RestController
public class AsyncController {

    @Autowired
    private AsyncServiceImpl asyncServiceImpl;

    @PostMapping("/test")
    public void test(){
        asyncServiceImpl.execute(5);
    }
}
